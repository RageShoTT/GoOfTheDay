import os
import random
import datetime
import time
import asyncio

import discord
from dotenv import load_dotenv

#get enviroment variables
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')#recover your own bot token
GUILD = os.getenv('DISCORD_GUILD')#recover server ID

#Permissions
intents = discord.Intents.default()
intents.message_content = True
intents.typing = True
intents.presences = True
intents.members = True

#start client
client = discord.Client(intents=intents,guilds=True)

memberArray = []
memberArrayCorrected = []
sample = []

#trigger at client.run(token)
@client.event
async def on_ready():
    #check all discord you are in, in this case only one
    for guild in client.guilds:

        #Remove dom
        dom = guild.get_role(1082262076287877210)
        doms = dom.members
        for currentDoms in doms:
            await currentDoms.remove_roles(dom)
            
        #Remove sub
        sub = guild.get_role(1082262186174468197)
        subs = sub.members
        for currentSubs in subs:
            await currentSubs.remove_roles(sub)

        #Remove Owner
        owner = guild.get_role(1082262259646079036)
        owners = owner.members
        for currentOwners in owners:
            await currentOwners.remove_roles(owner)
        
        #Remove Pet
        pet = guild.get_role(1082262319440089149)
        pets = pet.members
        for currentPets in pets:
            await currentPets.remove_roles(pet)

        #Remove Furry 
        furry = guild.get_role(1113417467189006396)
        furries = furry.members
        for currentFurries in furries:
            await currentFurries.remove_roles(furry)

        #Remove Femboy
        femboy = guild.get_role(1205893165656182815)
        femboys = femboy.members
        for currentFemboys in femboys:
            await currentFemboys.remove_roles(femboy)

        #Remove Cutie
        cutie = guild.get_role(1206645344314720306)
        cuties = cutie.members
        for currentCuties in cuties:
            await currentCuties.remove_roles(cutie)

        #List all user to then sample
        listed = guild.get_role(1075019566775803944)
        listedMembers = listed.members
        for membro in listedMembers:
            if not membro.bot:
                memberArray.append('<@' + str(membro.id) + '>')

    #Filter none because Discord API sucks
    memberArrayCorrected = list(filter(None,memberArray))
    channel = client.get_channel(1073194822489350254)#Get Channel to write on in the guild

    sample = random.sample(memberArrayCorrected,7)#Get seven uniques IDs

    #Assign the role based on which one of the sample is picked up in which position
    #Assign Dommy
    for membro in listedMembers:
        if sample[0] == '<@' + str(membro.id) + '>':
            await membro.add_roles(dom)

    #Assign Sub
    for membro in listedMembers:
        if sample[1] == '<@' + str(membro.id) + '>':
            await membro.add_roles(sub)
    
    #Assign Owner
    for membro in listedMembers:
        if sample[2] == '<@' + str(membro.id) + '>':
            await membro.add_roles(owner)

    #Assign Pet
    for membro in listedMembers:
        if sample[3] == '<@' + str(membro.id) + '>':
            await membro.add_roles(pet)

    #Assign Furry
    for membro in listedMembers:
        if sample[4] == '<@' + str(membro.id) + '>':
            await membro.add_roles(furry)
    
    #Assign Femboy
    for membro in listedMembers:
        if sample[5] == '<@' + str(membro.id) + '>':
            await membro.add_roles(femboy)

    #Assign Cutie
    for membro in listedMembers:
        if sample[6] == '<@' + str(membro.id) + '>':
            await membro.add_roles(cutie)

    #send message to the channel
    await channel.send(f"Dom Of The Day: {sample[0]}\nSub Of The Day: {sample[1]}\nOwner Of The Day: {sample[2]}\nPet Of The Day: {sample[3]}\nFurry Of The Day: {sample[4]}\nFemboy Of The Day: {sample[5]}\nCutie Of The Day: {sample[6]}\n")

#main one minute loop
while True:
    now = datetime.datetime.now()#get time
    #if '09:00' in str(now.time()): #if time is the correct one to post and edit roles with
    client.run(TOKEN)#run script
    time.sleep(60)#wait a minute