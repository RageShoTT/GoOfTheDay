
# OfTheDay

Select 7 random non bot valid user from server and set their role correctly each day at 9AM removing the previous assigned role.

# Current status

- [x]  Randomly pic 7 users from the user list
- [x]  Add timer
- [x]  Remove current roles
- [x]  Assign user their correct role

# To do Eventually

- [ ]  Refactor code in a decent way that isn't unoptimized crap like it is right now.
- [ ]  Add more stuff to make the random more interesting
- [ ]  Dynamic select the daily role and number of daily selection
